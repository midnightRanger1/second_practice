def string_analyse(): 
    is_string = False
    some_string = "SOME_STRING"

    true_choice = False
    choice = 0
    while is_string==False:
        try: 
            some_string = input("Введите строчку: ")
        except ValueError: 
            print("Ошибка: Формат строки неверный")
        else:
            string_length = len(some_string)
            comma_inc = 0
            space_inc = 0
            for i in range(string_length):
                if(some_string[i] == ','):
                    comma_inc +=1
                elif(some_string[i]==" "):
                    space_inc+=1
            is_string=True
            print ("Количество символов: ", string_length)
            print ("Количество запятых: ", comma_inc)
            print ("Количество пробелов: ", space_inc)
            print("Вернуться в меню или повторить операцию? 1 - Вернуться, 2 - Повторить")
            while true_choice == False:
                choice = correct_input("Выберите операцию: ")
                if(choice == 1):
                 true_choice = True
                 main_menu()
                elif(choice == 2):
                  true_choice = True
                  string_analyse()