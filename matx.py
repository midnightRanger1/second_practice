def matrix_create():
     true_choice = False
     choice = 0
     first_step = True
     mtx_rows = correct_int_input("Введите кол-во столбцов: ")
     mtx_columns = correct_int_input("Введите кол-во строк: ")
     start_value = correct_int_input("Введите начальное значение: ")
     mtx_step = correct_int_input("Введите шаг матрицы: ")
     mtx = []
  
     last_element = 0 

     for i in range(mtx_rows):
         a = []
         for j in range(mtx_columns):
             if(first_step == True):
                 first_step = False
                 a.append(start_value)
                 last_element = start_value
             else:
                 last_element += mtx_step
                 a.append(last_element)
         mtx.append(a)
     printMatrix(mtx)
    
     print("Вернуться в меню или повторить операцию? 1 - Вернуться, 2 - Повторить")
     while true_choice == False:
            choice = correct_input("Выберите операцию: ")
            if(choice == 1):
                true_choice = True
                main_menu()
            elif(choice == 2):
                true_choice = True
                matrix_create()

